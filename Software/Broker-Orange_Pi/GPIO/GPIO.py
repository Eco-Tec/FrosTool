#! /usr/bin/python3
#-*- coding: utf-8 -*-

import os

class GPIO():
    
    def __init__(self):
        self.ruta="/sys/class/gpio"
        
    def export(self,gpio):
        try:
            os.system("echo "+gpio+" > "+self.ruta+"/export")
        except:
            pass
        
    def unexport(self,gpio):
        try:
            os.system("echo "+gpio+" > "+self.ruta+"/unexport")
        except:
            pass
        
    def direction(self,gpio,direction):
        os.system("echo "+direction+" > "+self.ruta+"/gpio"+gpio+"/direction")
        
           
    def input(self,gpio):
        path = "/sys/class/gpio/gpio"+gpio+"/value"
        with open(path, "r") as fp:
            value = fp.read()
            if value.strip() == str(0):
                return 0
            else:
                return 1
        #return os.system("cat /"+self.ruta+"/gpio"+gpio+"/value")
        
    def write(self,gpio,value):
        #print("echo "+value+" > "+self.ruta+"/gpio"+gpio+"/value")
        os.system("echo "+value+" > "+self.ruta+"/gpio"+gpio+"/value")
    
