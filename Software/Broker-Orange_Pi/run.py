#!/usr/bin/env python3
#-*- coding: utf-8 -*-
"""
    DESCRIPCI?N...
    This software is free, licensed y distributed under GPL v3.
    (see COPYING) WITHOUT ANY WARRANTY.
    You can see a description of license here: http://www.gnu.org/copyleft/gpl.html
    Copyright(c) 2017 by fandres "Fabian Salamanca" <fabian.salamanca@openmailbox.org>
    Distributed under GPLv3+
    Based On https://github.com/fandres/Monitor-heladas/blob/master/Code/main.py
    Author  - Fabian A. Salamanca F (@fandres).
            - Marlon mauricio Moreno (@mau_rinc2010).
    Hardware: ORANGE PI ZERO
"""
NAME = "BROKER_FROSTOOL"
VERSION = "1.0"
SUBVERSION = "beta.1"

#from GUI.source.main import screen
from MQTT.main import main

import os
import sys


#sys.path.append(os.getcwd()) 


if __name__ == '__main__':
    # configurando RTC.ALARM0
    
    ruta = "/home/Software/Broker-Orange_Pi" #os.getcwd()
    mqtt = main(ruta)
    #screen = screen(ruta)
    
