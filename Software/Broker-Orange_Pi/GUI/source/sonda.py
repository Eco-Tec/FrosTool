#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# Creation:    26.05.2013
# Last Update: 07.04.2015
#
# Copyright (c) 2013-2015 by Georg Kainzbauer <http://www.gtkdb.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 


from .widget import*
from .RGB   import*

class sonda():
    def __init__(self,screen,path):
        self.screen=screen
        self.list_view={}
        self.sonda=""
        self.sondas={}
        self.backcolor=BLACK
        self.path =path
        self.plantilla()
        self.con = False

    def set_path(self, path):
        self.path = path
        print (self.path)
    
    def set_sonda(self,name):
        self.sonda=name
        
    def new_sonda(self,name):
        return {"VAL_TEM":"0.0","VAL_HUM":"0.0","TITLE":name,"DEBUG":" " ,"Time":0,"CONE":self.path+"/sonda_off.png","ESTA":False}
        
    def add_sonda(self,name):
        self.sondas[name]=self.new_sonda(name)
        
    def set_backcolor(self,color):
        self.backcolor=color
        
    def set_dato_sonda(self,name_sonda,name_variable,dato):            
        self.sondas[name_sonda][name_variable]=dato[0:4]
        self.reset_time(name_sonda)
        self.sondas[name_sonda]["ESTA"]=True
        #self.set_icon_connect(name_sonda,True)
        self.set_msm_debug(name_sonda,"")
        self.refresh_all()

    def set_msm_debug(self,name_sonda,msm):
        self.sondas[name_sonda]["DEBUG"]=str(msm)

    def add_time(self,name_sonda):
        self.sondas[name_sonda]["Time"]=self.sondas[name_sonda]["Time"]+1
        if self.sondas[name_sonda]["Time"]<=100:
            if self.sondas[name_sonda]["ESTA"]:
                self.set_msm_debug(name_sonda,"")
                self.set_icon_connect(name_sonda,True)
            #else:
             #   self.set_msm_debug(name_sondam,"")
             #   self.set_icon_connect(name_sonda,False)
        else:
            self.set_icon_connect(name_sonda,False)
            self.set_msm_debug(name_sonda,"Error de conexión")

    def set_icon_connect(self,name_sonda,estado):
        if estado:
            self.sondas[name_sonda]["CONE"]=self.path+"/sonda.png"
        else:
            self.sondas[name_sonda]["CONE"]=self.path+"/sonda_off.png"

    def reset_time(self,name_sonda):
        self.set_icon_connect(name_sonda,True)
        self.sondas[name_sonda]["Time"]=0
        self.set_time()
        
    def add_label(self, name, pos,text,size,color,fondo):
        label=widget_text(self.screen, name,pos[0],pos[1],text,size,color,back_color=fondo,global_color=self.backcolor)
        #self.variables[name]=text
        self.list_view[name]=label
        
    def add_imagen(self, name, ruta, escala,pos,angulo):
        imagen=widget_imagen(self.screen, ruta,escala,name,pos[0],pos[1],angulo,BLACK)
        self.list_view[name]=imagen
        imagen.scale()
        imagen.update()

    def set_connect(self):
        a=self.list_view["TITLE_I"]
        a.load_file(self.sondas[self.sonda]["CONE"])
        a.scale()
        a.update()

    def set_date(self,date):
        a=self.list_view["Fecha"]
        a.set_text(date)

    def set_time(self):
        a=self.list_view["Time"]
        a.set_text(str(self.sondas[self.sonda]["Time"]))

    def set_debug(self):
        a=self.list_view["DEBUG"]
        a.set_text(self.sondas[self.sonda]["DEBUG"])
    
    def set_temp(self):
    #    self.variables[name]=text
        a=self.list_view["VAL_TEM"]
        a.set_text(self.sondas[self.sonda]["VAL_TEM"]) 
        
    def set_hum(self):
    #    self.variables[name]=text
        a=self.list_view["VAL_HUM"]
        a.set_text(self.sondas[self.sonda]["VAL_HUM"]) 
    
    def set_title(self):
    #    self.variables[name]=text
        a=self.list_view["TITLE"]
        a.set_text(self.sondas[self.sonda]["TITLE"])
        
    def set_temp_u(self):
    #    self.variables[name]=text
        a=self.list_view["TEM_U"]
        a.set_text(chr(176)+"C")    
        
    def set_hum_u(self):
    #    self.variables[name]=text
        a=self.list_view["HUM_U"]
        a.set_text("%")    

    def clear_all(self):
        for b,a in self.list_view.items():
            a.delete()        
        
    def refresh_all(self):
        self.set_temp()
        self.set_hum()
        self.set_title()
        self.set_time()
        self.set_debug()
        self.set_connect()
        #self.set_temp_u()
        #self.set_hum_u()
        
    def plantilla(self):
        self.add_imagen("TITLE_I",self.path+"/sonda_off.png",[48,48],[2,2],0)
        self.add_label("TITLE",[60,10],"",50,WHITE,BLACK)
        self.add_imagen("TEM_U",self.path+"/celsius.png",[32,32],[190,75],0)
        self.add_imagen("TEM",self.path+"/termo.png",[48,48],[2,65],0)
        #self.add_label("TEM_U",[260,60],chr(176)+"C",50,WHITE,BLACK)
        self.add_imagen("HUM",self.path+"/humedad.png",[48,48],[2,130],0)
        self.add_imagen("HUM_U",self.path+"/porcen.png",[32,32],[190,135],0)
        self.add_label("VAL_TEM",[60,65],"", 85,WHITE,BLACK)
        self.add_label("VAL_HUM",[60,125],"",85,WHITE,BLACK)
        self.add_label("DEBUG",[2,220],"",30,WHITE,BLACK)
        self.add_label("Time",[285,220],"",30,WHITE,BLACK)
        self.add_imagen("Time_u",self.path+"/actua.png",[32,32],[248,210],0)
        self.add_label("Fecha",[260,5],"",30,WHITE,BLACK)
        
