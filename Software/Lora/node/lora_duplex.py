import time
from sx127x import config

import machine
import esp
import dht

msgCount = 0            # count of outgoing messages
INTERVAL = 2000         # interval between sends
INTERVAL_BASE = 2000    # interval between sends base
s_dht = dht.DHT22(machine.Pin(22))  # Pin DNT22


def duplex_callback(lora):
    print("LoRa Duplex with callback")
    lora.onReceive(on_receive)  # register the receive callback
    do_loop(lora)


def do_loop(lora):
    global msgCount
    global s_dht

    lastSendTime = 0
    interval = 0

    while True:
        now = config.millisecond()
        if now < lastSendTime:
            lastSendTime = now

        if (now - lastSendTime > interval):
            lastSendTime = now                                      # timestamp the message
            interval = (lastSendTime % INTERVAL) + INTERVAL_BASE    # 2-3 seconds

            s_dht.measure()     # Senspr
            temperatura = s_dht.temperature()
            s_dht.humidity()
            humedad = s_dht.humidity()
            print("Temperatura", s_dht.temperature(), "Humedad", s_dht.humidity())  # Debug

            message = "{} {}".format(temperatura, humedad, config.CONFIG.NODE_NAME)
            send_message(lora, message)                              # send message
            msgCount += 1

            lora.receive()                                          # go into receive mode


def send_message(lora, outgoing):
    lora.println(outgoing)
    print("Sending message:\n{}\n".format(outgoing))


def on_receive(lora, payload):
    lora.blink_led()
    payload_string = payload.decode()
    rssi = lora.packetRssi()
    print("*** Received message ***\n{}".format(payload_string))
    if config.CONFIG.IS_TTGO_LORA_OLED:
        lora.show_packet(payload_string, rssi)
    print("with RSSI {}\n".format(rssi))
